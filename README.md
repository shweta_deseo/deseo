Setup
-
Install typescript globally

		npm install -g typescript

Follow the tutorial at the following link for more clarity about using typescript with VSCode.

https://code.visualstudio.com/docs/typescript/typescript-tutorial

Extensions
-
1. EditorConfig. The config file is already created

2. GitLens

3. Prisma

4. Prettier Now

5. Remote SSH ( from microsoft. both available extensions )
clone the starter kit

6. Visual studio Intellicode

7. vscode-icons

8. ESLint

Installation
-

Clone this repo

cd to the dir and run the following commands

		npm install

#husky and lint-staged

		npx mrm lint-staged

The above shoud set up the required environment for typescript.

We are using Eslint, Prettier, EditorConfig, Husky

Eslint/Prettier is going to follow the coding standards and rule of airbnb, It is alread configured to do that.

Check out the package.json to understand the evironment

		npm run build ( to build the code )
		npm run lint ( to run the linter)

on git commit the linter will run on the committed code

Note
-

1. The .evn file is used to set the environment variables.
It is expected to be in the folder from the code is invoked ( Current Working directory )
Git ignores the .env file while uploadin to the repo. it is good practice
Hence we will update the settings to envtemplate.txt

2. Use nvm to install node and manage node versions. Do not install Node on the system directly.

3. Remeber to set the autosave option in VSCode in the file menu.

Things to TODO for the team
-
1. All APIs that are publicly consumed have to be written according to the  folder structure

		model
		service
		controller
		router

2. Checkout the logger ( morgan ) and implement the same to the project

3. Please learn the use of the spread operator

4. Use ATLAS service to conenct to mongodb

5. Figure out how to generate swagger doc for the created APIs.

6. Learn docker basics.



