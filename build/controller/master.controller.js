"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPublisherTypeById = exports.getpublisherType = exports.createPublisherType = exports.updateAssetClassById = exports.getAssetClassById = exports.getAssetClass = exports.createAssetClass = exports.postservicetype = exports.getservicetype = void 0;
const MasterService = require("../service/master.service");
const getservicetype = async (request, response) => {
    MasterService.GetServiceType(request, response);
};
exports.getservicetype = getservicetype;
const postservicetype = async (request, response) => {
    MasterService.PostServiceType(request, response);
};
exports.postservicetype = postservicetype;
const createAssetClass = async (request, response) => {
    MasterService.postAssetClass(request, response);
};
exports.createAssetClass = createAssetClass;
const getAssetClass = async (request, response) => {
    MasterService.getAssetClass(request, response);
};
exports.getAssetClass = getAssetClass;
const getAssetClassById = async (request, response) => {
    MasterService.getAssetClassById(request, response);
};
exports.getAssetClassById = getAssetClassById;
const updateAssetClassById = async (request, response) => {
    MasterService.updateAssetClassById(request, response);
};
exports.updateAssetClassById = updateAssetClassById;
const createPublisherType = async (request, response) => {
    MasterService.postPublisherType(request, response);
};
exports.createPublisherType = createPublisherType;
const getpublisherType = async (request, response) => {
    MasterService.getPublisherType(request, response);
};
exports.getpublisherType = getpublisherType;
const getPublisherTypeById = async (request, response) => {
    MasterService.getPublisherTypeById(request, response);
};
exports.getPublisherTypeById = getPublisherTypeById;
//# sourceMappingURL=master.controller.js.map