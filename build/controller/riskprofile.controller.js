"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postrpcategorytype = exports.getrpcategorytype = void 0;
const RPService = require("../service/riskprofile.service");
const getrpcategorytype = async (request, response) => {
    RPService.GetCategoryType(request, response);
};
exports.getrpcategorytype = getrpcategorytype;
const postrpcategorytype = async (request, response) => {
    RPService.PostRPCategoryType(request, response);
};
exports.postrpcategorytype = postrpcategorytype;
//# sourceMappingURL=riskprofile.controller.js.map