"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Putpublisherprofile = exports.Getpublisherprofilebyid = exports.Postpublisherprofile = exports.Getpublisherprofile = exports.Putpublisher = exports.Postpublisher = exports.Getpublisherbyid = exports.Getpublisher = void 0;
const PublisherService = require("../service/publisher.service");
const Getpublisher = async (request, response) => {
    PublisherService.Getpublisher(response);
};
exports.Getpublisher = Getpublisher;
const Getpublisherbyid = async (request, response) => {
    PublisherService.Getpublisherbyid(request, response);
};
exports.Getpublisherbyid = Getpublisherbyid;
const Postpublisher = async (request, response) => {
    PublisherService.Postpublisher(request, response);
};
exports.Postpublisher = Postpublisher;
const Putpublisher = async (request, response) => {
    PublisherService.Putpublisher(request, response);
};
exports.Putpublisher = Putpublisher;
const Getpublisherprofile = async (request, response) => {
    PublisherService.Getpublisherprofile(request, response);
};
exports.Getpublisherprofile = Getpublisherprofile;
const Postpublisherprofile = async (request, response) => {
    PublisherService.Postpublisherprofile(request, response);
};
exports.Postpublisherprofile = Postpublisherprofile;
const Getpublisherprofilebyid = async (request, response) => {
    PublisherService.Getpublisherprofilebyid(request, response);
};
exports.Getpublisherprofilebyid = Getpublisherprofilebyid;
const Putpublisherprofile = async (request, response) => {
    PublisherService.Putpublisherprofile(request, response);
};
exports.Putpublisherprofile = Putpublisherprofile;
//# sourceMappingURL=publisher.controller.js.map