"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getuser = exports.login = exports.signup = void 0;
const UserService = require("../service/user.service");
const signup = async (request, response) => {
    UserService.signup(request, response);
};
exports.signup = signup;
const login = async (request, response) => {
    UserService.login(request, response);
};
exports.login = login;
const getuser = async (request, response) => {
    UserService.getuser(request, response);
};
exports.getuser = getuser;
//# sourceMappingURL=user.controller.js.map