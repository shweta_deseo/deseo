"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cors = require("cors");
const dotenv = require("dotenv");
const express = require("express");
const helmet = require("helmet");
// import * as morgan from 'morgan';
// import * as swaggerUi from 'swagger-ui-express';
dotenv.config();
if (!process.env.PORT) {
    process.exit(1);
}
const PORT = parseInt(process.env.PORT, 10);
const app = express();
app.use(helmet());
app.use(cors());
app.use(express.json());
// app.use(morgan('tiny'));
// morgan.token('type', function (req, res) { return req.headers['content-type'] })
// Added By Shweta to connect with routes
const userRoutes = require('./routes/user.route');
app.use('/user', userRoutes);
const masterRoutes = require('./routes/master.route');
app.use('/master', masterRoutes);
const rpRoutes = require('./routes/riskprofile.route');
app.use('riskprofile', rpRoutes);
const publisherRoutes = require('./routes/publisher.route');
app.use('/publisher', publisherRoutes);
/* app.use(express.static('public'));

app.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
        swaggerOptions: {
            url: '/swagger.json',
        },
    }),
);
 */
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
//# sourceMappingURL=server.js.map