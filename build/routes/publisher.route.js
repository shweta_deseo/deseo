// added by parth
const pexpress = require('express');
const prouter = pexpress.Router();
const publisherController = require('../controller/publisher.controller');
prouter.post('/masterdetails', 
// joiSchemaValidation.validateBody(userSchema.signup),
publisherController.Postpublisher);
prouter.get('/masterdetails', 
// joiSchemaValidation.validateBody(userSchema.login),
publisherController.Getpublisher);
prouter.get('/masterdetails/:id', 
// joiSchemaValidation.validateBody(userSchema.login),
publisherController.Getpublisherbyid);
prouter.patch('/masterdetails', 
// joiSchemaValidation.validateBody(userSchema.login),
publisherController.Putpublisher);
prouter.post('/profile', 
// joiSchemaValidation.validateBody(userSchema.signup),
publisherController.Postpublisherprofile);
prouter.get('/profile', 
// joiSchemaValidation.validateBody(userSchema.login),
publisherController.Getpublisherprofile);
prouter.get('/profile/:id', 
// joiSchemaValidation.validateBody(userSchema.login),
publisherController.Getpublisherprofilebyid);
prouter.patch('/profile', 
// joiSchemaValidation.validateBody(userSchema.login),
publisherController.Putpublisherprofile);
module.exports = prouter;
//# sourceMappingURL=publisher.route.js.map