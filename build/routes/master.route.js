/* import express from 'express';

import * as userController from '../controller/user.controller'; */
// Changed By Shweta from import to require
const mexpress = require('express');
const mrouter = mexpress.Router();
// Changed By Shweta from import to require
const masterController = require('../controller/master.controller');
mrouter.post('/serviceType', 
// joiSchemaValidation.validateBody(userSchema.signup),
masterController.postservicetype);
mrouter.get('/serviceType', 
// joiSchemaValidation.validateBody(userSchema.login),
masterController.getservicetype);
mrouter.post('/AssetClass/create', 
// joiSchemaValidation.validateBody(userSchema.signup),
masterController.createAssetClass);
mrouter.get('/AssetClass/', masterController.getAssetClass);
mrouter.get('/AssetClass/:id', masterController.getAssetClassById);
mrouter.put('/AssetClass/:id', masterController.updateAssetClassById);
mrouter.post('/PublisherType/create', 
// joiSchemaValidation.validateBody(userSchema.signup),
masterController.createPublisherType);
mrouter.get('/PublisherType/', masterController.getpublisherType);
// Added By Shweta to access route in Server.ts
module.exports = mrouter;
//# sourceMappingURL=master.route.js.map