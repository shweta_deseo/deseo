"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updatePublisherTypeById = exports.getPublisherTypeById = exports.getPublisherType = exports.postPublisherType = exports.updateAssetClassById = exports.getAssetClassById = exports.getAssetClass = exports.postAssetClass = exports.GetServiceType = exports.PostServiceType = void 0;
const client_1 = require("@prisma/client");
const index_1 = require("../constants/index");
const prisma = new client_1.PrismaClient();
const PostServiceType = async (req, res) => {
    const { typeCode, typeName, description, logo, typeStatus } = req.body;
    const serviceType = await prisma.serviceType.create({
        data: {
            typeCode,
            typeName,
            description,
            logo,
            typeStatus,
        },
    });
    res.json(serviceType);
};
exports.PostServiceType = PostServiceType;
const GetServiceType = async (req, res) => {
    // const { id } = req.body;
    const serviceType = await prisma.serviceType.findMany();
    res.json(serviceType);
};
exports.GetServiceType = GetServiceType;
const postAssetClass = async (req, res) => {
    const { classCode, className, classStatus } = req.body;
    const assetClass = await prisma.mstAssetClass.create({
        data: {
            classCode,
            className,
            classStatus,
        },
    });
    res.json(assetClass);
};
exports.postAssetClass = postAssetClass;
const getAssetClass = async (req, res) => {
    const assetClasses = await prisma.mstAssetClass.findMany();
    console.log(assetClasses);
    res.json(assetClasses);
};
exports.getAssetClass = getAssetClass;
const getAssetClassById = async (req, res) => {
    const { id } = req.params;
    const assetClasses = await prisma.mstAssetClass.findUnique({
        where: { id: id },
    });
    // const code  = req.params.classCode;
    console.log('input: ', req.params);
    // const assetClasses = await prisma.mstAssetClass.findFirst({where:{classCode:code}})
    if (assetClasses) {
        return res.status(200).json(assetClasses);
        // return res.status(200).json({ data: assetClasses });
    }
    else {
        return res
            .status(404)
            .json({ Message: index_1.assetClass.assetClassMessage.ASSETCLASS_NOT_FOUND });
    }
    console.log(assetClasses);
};
exports.getAssetClassById = getAssetClassById;
const updateAssetClassById = async (req, res) => {
    const { id } = req.params;
    const { classCode, className, classStatus } = req.body;
    let assetClasses = await prisma.mstAssetClass.findFirst({
        where: { id: id },
    });
    if (!assetClasses)
        return res
            .status(404)
            .json({ Message: index_1.assetClass.assetClassMessage.ASSETCLASS_NOT_FOUND });
    await prisma.mstAssetClass.update({
        where: { id: id },
        data: { classCode, className, classStatus },
    });
    return res.status(200).json(assetClasses);
    // return res.status(200).json({ data: assetClasses });
};
exports.updateAssetClassById = updateAssetClassById;
const postPublisherType = async (req, res) => {
    const { typeCode, typeName, typeStatus } = req.body;
    const publisherType = await prisma.publisherType.create({
        data: {
            typeCode,
            typeName,
            typeStatus,
        },
    });
    res.json(publisherType);
};
exports.postPublisherType = postPublisherType;
const getPublisherType = async (req, res) => {
    const publisherTypesDetails = await prisma.publisherType.findMany();
    console.log(publisherTypesDetails);
    res.json(publisherTypesDetails);
};
exports.getPublisherType = getPublisherType;
const getPublisherTypeById = async (req, res) => {
    const { id } = req.params;
    const publisherTypesDetails = await prisma.publisherType.findUnique({
        where: { id: id },
    });
    console.log(publisherTypesDetails);
    res.json(publisherTypesDetails);
};
exports.getPublisherTypeById = getPublisherTypeById;
const updatePublisherTypeById = async (req, res) => {
    const { id } = req.params;
    const { typeCode, typeName, typeStatus } = req.body;
    let publisherTypesDetails = await prisma.publisherType.findFirst({
        where: { id: id },
    });
    if (!publisherTypesDetails)
        return res.status(404).json({
            Message: index_1.mstPublisherType.publisherTypeMessage.PUBLISHERTYPE_NOT_FOUND,
        });
    await prisma.publisherType.update({
        where: { id: id },
        data: { typeCode, typeName, typeStatus },
    });
    publisherTypesDetails = await prisma.publisherType.findFirst({
        where: { id: id },
    });
    return res.status(200).json(publisherTypesDetails);
    // return res.status(200).json({ data: assetClasses });
};
exports.updatePublisherTypeById = updatePublisherTypeById;
//# sourceMappingURL=master.service.js.map