"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Putpublisherprofile = exports.Getpublisherprofilebyid = exports.Postpublisherprofile = exports.Getpublisherprofile = exports.Putpublisher = exports.Postpublisher = exports.Getpublisherbyid = exports.Getpublisher = void 0;
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
// #region PublisherMaster
const Getpublisher = async (res) => {
    try {
        const pub = await prisma.publisherMaster.findMany();
        console.log(pub);
        res.json(pub);
    }
    catch (err) {
        res.json(err);
    }
};
exports.Getpublisher = Getpublisher;
const Getpublisherbyid = async (req, res) => {
    const { id } = req.params;
    try {
        const pub = await prisma.publisherMaster.findUnique({
            where: {
                id,
            },
            // select:{publishercode : true }, //this will fetch only particular column
        });
        res.json(pub);
    }
    catch (err) {
        res.json(err);
    }
};
exports.Getpublisherbyid = Getpublisherbyid;
const Postpublisher = async (req, res) => {
    const { publishercode, publishertaxstatus, publishername, publishertaxidno, publishergstno, countryid, publishertypeid, publisherstatus, createdby, updatedby, updateddate, createddate, } = req.body;
    try {
        const country = await prisma.country.findUnique({
            where: {
                id: countryid,
            },
        });
        const publisherType = await prisma.publisherType.findUnique({
            where: {
                id: publishertypeid,
            },
        });
        if (country.id != null && publisherType.id != null) {
            const pub = await prisma.publisherMaster.create({
                data: {
                    publishercode,
                    publishertaxstatus,
                    publishername,
                    publishertaxidno,
                    publishergstno,
                    countryid,
                    publishertypeid,
                    publisherstatus,
                    createdby,
                    updatedby,
                    updateddate,
                    createddate,
                },
            });
            res.json(pub);
        }
        res.json(req);
    }
    catch (err) {
        res.json(err);
    }
};
exports.Postpublisher = Postpublisher;
const Putpublisher = async (req, res) => {
    const { id, publishercode, publishertaxstatus, publishername, publishertaxidno, publishergstno, countryid, publishertypeid, publisherstatus, createdby, updatedby, } = req.body;
    try {
        const pub = await prisma.publisherMaster.update({
            where: {
                id,
            },
            data: {
                publishercode,
                publishertaxstatus,
                publishername,
                publishertaxidno,
                publishergstno,
                countryid,
                publishertypeid,
                publisherstatus,
                createdby,
                updatedby,
            },
        });
        res.json(pub);
    }
    catch (err) {
        res.json(err);
    }
};
exports.Putpublisher = Putpublisher;
// #endregion
// #region PublisherProfile
const Getpublisherprofile = async (req, res) => {
    try {
        const pub = await prisma.publisherProfile.findMany();
        console.log(pub);
        res.json(pub);
    }
    catch (err) {
        res.json(err);
    }
};
exports.Getpublisherprofile = Getpublisherprofile;
const Postpublisherprofile = async (req, res) => {
    const { publishermasterid, contactperson, mobile, mobileverified, emailverified, accountemailid, address1, address2, zipcode, country, logo, publisherdescription, whitelabelledsubdomainname, whitelabelledmobileappname, website, facebooklink, linkedinlink, termsncondition, publisherstatus, createdby, updatedby, createddate, updateddate, } = req.body;
    try {
        const publishermaster = await prisma.publisherMaster.findUnique({
            where: {
                id: publishermasterid,
            },
        });
        console.log(publishermaster);
        if (publishermaster.id != null) {
            const pub = await prisma.publisherProfile.create({
                data: {
                    publishermasterid,
                    contactperson,
                    mobile,
                    mobileverified,
                    emailverified,
                    accountemailid,
                    address1,
                    address2,
                    zipcode,
                    country,
                    logo,
                    publisherdescription,
                    whitelabelledsubdomainname,
                    whitelabelledmobileappname,
                    website,
                    facebooklink,
                    linkedinlink,
                    termsncondition,
                    publisherstatus,
                    createdby,
                    updatedby,
                    createddate,
                    updateddate,
                },
            });
            console.log(pub);
            res.json(pub);
        }
        console.log(req);
        res.json(req);
    }
    catch (err) {
        console.log(err);
        res.json(req);
    }
};
exports.Postpublisherprofile = Postpublisherprofile;
const Getpublisherprofilebyid = async (req, res) => {
    const { id } = req.params;
    try {
        const pub = await prisma.publisherProfile.findUnique({
            where: {
                id,
            },
        });
        res.json(pub);
    }
    catch (err) {
        res.json(err);
    }
};
exports.Getpublisherprofilebyid = Getpublisherprofilebyid;
const Putpublisherprofile = async (req, res) => {
    const { id, publishermasterid, contactperson, mobile, mobileverified, emailverified, accountemailid, address1, address2, zipcode, country, logo, publisherdescription, whitelabelledsubdomainname, whitelabelledmobileappname, website, facebooklink, linkedinlink, termsncondition, publisherstatus, createdby, updatedby, createddate, updateddate, } = req.body;
    try {
        const pub = await prisma.publisherProfile.update({
            where: {
                id,
            },
            data: {
                publishermasterid,
                contactperson,
                mobile,
                mobileverified,
                emailverified,
                accountemailid,
                address1,
                address2,
                zipcode,
                country,
                logo,
                publisherdescription,
                whitelabelledsubdomainname,
                whitelabelledmobileappname,
                website,
                facebooklink,
                linkedinlink,
                termsncondition,
                publisherstatus,
                createdby,
                updatedby,
                createddate,
                updateddate,
            },
        });
        res.json(pub);
    }
    catch (err) {
        res.json(err);
    }
};
exports.Putpublisherprofile = Putpublisherprofile;
// #endregion
//# sourceMappingURL=publisher.service.js.map