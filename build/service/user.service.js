"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getuser = exports.login = exports.signup = void 0;
/* eslint-disable prettier/prettier */
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
/**
 * Service Methods
 */
const signup = async (req, res) => {
    const { name, email } = req.body;
    const user = await prisma.user.create({
        data: {
            name,
            email,
        },
    });
    res.json(user);
};
exports.signup = signup;
// eslint-disable-next-line no-unused-vars
const login = async (req, res) => { };
exports.login = login;
const getuser = async (req, res) => {
    const users = await prisma.user.findMany();
    console.log(users);
    res.json(users);
};
exports.getuser = getuser;
//# sourceMappingURL=user.service.js.map