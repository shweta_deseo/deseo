"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetCategoryType = exports.PostRPCategoryType = void 0;
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
const PostRPCategoryType = async (req, res) => {
    try {
        const { categoryTypeName, description, externalmapcode, checkersReason, createdBy, createdDate, } = req.body;
        const categoryType = await prisma.rPCategoryType.create({
            data: {
                categoryTypeName,
                description,
                externalmapcode,
                checkersReason,
                createdBy,
                createdDate,
            },
        });
        res.json(categoryType);
    }
    catch (e) {
        // The .code property can be accessed in a type-safe manner
        console.log(e.message);
    }
};
exports.PostRPCategoryType = PostRPCategoryType;
const GetCategoryType = async (req, res) => {
    // const { id } = req.body;
    const categoryType = await prisma.rPCategoryType.findMany();
    res.json(categoryType);
};
exports.GetCategoryType = GetCategoryType;
//# sourceMappingURL=riskprofile.service.js.map