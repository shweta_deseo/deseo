/* eslint-disable prettier/prettier */
/*import express from 'express';

import * as userController from '../controller/user.controller';*/


// Changed By Shweta from import to require 
const express = require('express');

const router = express.Router();

// Changed By Shweta from import to require 
const userController = require('../controller/user.controller');

router.post(
	'/signup',
	// joiSchemaValidation.validateBody(userSchema.signup),
	userController.signup,
);
router.post(
	'/login',
	// joiSchemaValidation.validateBody(userSchema.login),
	userController.login,
);
router.get(
	'/',
	userController.getuser,
)


// Added By Shweta to access route in Server.ts 
module.exports=router;

// export default router;