export const httpCodes = {
	400: 'Bad Request -- The request could not be understood by the server due to malformed syntax',
	401: 'Unauthorized -- The request requires user authentication',
	403: 'Forbidden -- The server understood the request, but is refusing to fulfill it.',
	404: 'Not Found -- The server has not found anything matching the Request-URI.',
	405: 'Method Not Allowed -- The method specified in the Request-Line is not allowed for the resource identified by the Request-URI.',
	409: 'Duplicate Transaction -- Transaction with the given number is already in process.',
	429: 'Too Many Requests -- Request counts exceed our limit. Slow down!',
	500: 'Internal Server Error -- We had a problem with our server. Try again later.',
	503: "Service Unavailable -- We're temporarially offline for maintanance. Please try again later.",
};

export const date = {
	format: 'DD/MM/YYYY HH:mm:ss',
	errrorMessage: {
		error:
			'Invalid date. Expected date format DD/MM/YYYY HH:mm:ss ( 0 to 23 hour format )',
	},
};

export const defaultServerResponse = {
	status: 400,
	message: '',
	body: {},
};

export const user = {
	userMessage: {
		SIGNUP_SUCCESS: 'Signup Success',
		LOGIN_SUCCESS: 'Login Success',
		LOGOUT_SUCCESS: 'Logout Success',
		INVALID_PASSWORD: 'Incorrect Password',
		DUPLICATE_EMAIL: 'User already exist with given email',
		USER_FETCHED: 'User fetched successsully',
		USER_NOT_FOUND: 'User not found',
		USER_NOT_UPDATED: 'User not updated',
		USER_UPDATED: 'User updated successfully',
		USER_DETAILS_NOT_UPDATED: 'User Details not updated',
		USER_DETAILS_UPDATED: 'User Details updated successfully',
		USER_DETAILS_FETCHED: 'User Details fetched successfully',
		USER_DETAILS_NOT_FOUND: 'User Details not found',
		AGENT_DETAILS_NOT_FOUND: 'Agent details not found',
		ADDRESS_COUNT_BREACH: 'You can only have 4 addresses registered',
		ADDRESS_TAG_EXISTS: 'Address tag already exists',
	},
	entitlement: {
		ADMIN: 1,
		CLIENT: 3,
	},
};

export const requestValidationMessage = {
	BAD_REQUEST: 'Invalid fields',
	TOKEN_MISSING: 'Token missing from header',
	ACCESS_DENIED:
		'You do not have the privileges to perform the requested operation',
};

export const assetClass = {
	assetClassMessage: {
		ASSETCLASS_NOT_FOUND: 'Asset class Details not found',
	},
};

export const mstPublisherType = {
	publisherTypeMessage: {
		PUBLISHERTYPE_NOT_FOUND: 'Publisher Type Details not found',
	},
};

export const mstPublisherBank = {
	publisherBankMessage: {
		PUBLISHERBANK_NOT_FOUND: 'Publisher Bank Details not found',
	},
};
