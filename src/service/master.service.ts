import { PrismaClient } from '@prisma/client';
import { Request, Response } from 'express';
import { assetClass, mstPublisherType } from '../constants/index';

const prisma = new PrismaClient();

export const PostServiceType = async (req: Request, res: Response) => {
	const { typeCode, typeName, description, logo, typeStatus } = req.body;

	const serviceType = await prisma.serviceType.create({
		data: {
			typeCode,
			typeName,
			description,
			logo,
			typeStatus,
		},
	});
	res.json(serviceType);
};

export const GetServiceType = async (req: Request, res: Response) => {
	// const { id } = req.body;
	const serviceType = await prisma.serviceType.findMany();
	res.json(serviceType);
};

export const postAssetClass = async (req: Request, res: Response) => {
	const { classCode, className, classStatus } = req.body;

	const assetClass = await prisma.mstAssetClass.create({
		data: {
			classCode,

			className,

			classStatus,
		},
	});

	res.json(assetClass);
};

export const getAssetClass = async (req: Request, res: Response) => {
	const assetClasses = await prisma.mstAssetClass.findMany();
	console.log(assetClasses);
	res.json(assetClasses);
};

export const getAssetClassById = async (req: Request, res: Response) => {
	const { id } = req.params;
	const assetClasses = await prisma.mstAssetClass.findUnique({
		where: { id: id },
	});
	// const code  = req.params.classCode;
	console.log('input: ', req.params);
	// const assetClasses = await prisma.mstAssetClass.findFirst({where:{classCode:code}})
	if (assetClasses) {
		return res.status(200).json(assetClasses);
		// return res.status(200).json({ data: assetClasses });
	} else {
		return res
			.status(404)
			.json({ Message: assetClass.assetClassMessage.ASSETCLASS_NOT_FOUND });
	}
	console.log(assetClasses);
};

export const updateAssetClassById = async (req: Request, res: Response) => {
	const { id } = req.params;
	const { classCode, className, classStatus } = req.body;
	let assetClasses = await prisma.mstAssetClass.findFirst({
		where: { id: id },
	});
	if (!assetClasses)
		return res
			.status(404)
			.json({ Message: assetClass.assetClassMessage.ASSETCLASS_NOT_FOUND });
	await prisma.mstAssetClass.update({
		where: { id: id },
		data: { classCode, className, classStatus },
	});

	return res.status(200).json(assetClasses);
	// return res.status(200).json({ data: assetClasses });
};

export const postPublisherType = async (req: Request, res: Response) => {
	const { typeCode, typeName, typeStatus } = req.body;

	const publisherType = await prisma.publisherType.create({
		data: {
			typeCode,

			typeName,

			typeStatus,
		},
	});

	res.json(publisherType);
};

export const getPublisherType = async (req: Request, res: Response) => {
	const publisherTypesDetails = await prisma.publisherType.findMany();
	console.log(publisherTypesDetails);
	res.json(publisherTypesDetails);
};

export const getPublisherTypeById = async (req: Request, res: Response) => {
	const { id } = req.params;
	const publisherTypesDetails = await prisma.publisherType.findUnique({
		where: { id: id },
	});
	console.log(publisherTypesDetails);
	res.json(publisherTypesDetails);
};

export const updatePublisherTypeById = async (req: Request, res: Response) => {
	const { id } = req.params;
	const { typeCode, typeName, typeStatus } = req.body;
	let publisherTypesDetails = await prisma.publisherType.findFirst({
		where: { id: id },
	});
	if (!publisherTypesDetails)
		return res.status(404).json({
			Message: mstPublisherType.publisherTypeMessage.PUBLISHERTYPE_NOT_FOUND,
		});
	await prisma.publisherType.update({
		where: { id: id },
		data: { typeCode, typeName, typeStatus },
	});
	publisherTypesDetails = await prisma.publisherType.findFirst({
		where: { id: id },
	});
	return res.status(200).json(publisherTypesDetails);
	// return res.status(200).json({ data: assetClasses });
};
