/* eslint-disable prettier/prettier */
import { PrismaClient } from '@prisma/client';
import { Request, Response } from 'express';

const prisma = new PrismaClient();

/**
 * Service Methods
 */

const signup = async (req: Request, res: Response) => {
	const { name, email } = req.body;

	const user = await prisma.user.create({
		data: {
			name,

			email,
		},
	});

	res.json(user);
};

// eslint-disable-next-line no-unused-vars
const login = async (req: Request, res: Response) => {};

const getuser = async (req: Request, res: Response) => {	 
	const users = await prisma.user.findMany();
	console.log(users);
	res.json(users);
};


export { signup, login,getuser };
