import { Prisma, PrismaClient } from '@prisma/client';
import { Request, Response } from 'express';

const prisma = new PrismaClient();

export const PostRPCategoryType = async (req: Request, res: Response) => {
	try {
		const {
			categoryTypeName,
			description,
			externalmapcode,
			checkersReason,
			createdBy,
			createdDate,
		} = req.body;

		const categoryType = await prisma.rPCategoryType.create({
			data: {
				categoryTypeName,
				description,
				externalmapcode,
				checkersReason,
				createdBy,
				createdDate,
			},
		});
		res.json(categoryType);
	} catch (e) {
		// The .code property can be accessed in a type-safe manner
		console.log(e.message);
	}
};

export const GetCategoryType = async (req: Request, res: Response) => {
	// const { id } = req.body;
	const categoryType = await prisma.rPCategoryType.findMany();
	res.json(categoryType);
};
