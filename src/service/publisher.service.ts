import { PrismaClient } from '@prisma/client';
import { Request, Response } from 'express';

const prisma = new PrismaClient();
// #region PublisherMaster
export const Getpublisher = async (res: Response) => {
	try {
		const pub = await prisma.publisherMaster.findMany();
		console.log(pub);
		res.json(pub);
	} catch (err) {
		res.json(err);
	}
};
export const Getpublisherbyid = async (req: Request, res: Response) => {
	const { id } = req.params;
	try {
		const pub = await prisma.publisherMaster.findUnique({
			where: {
				id,
			},
			// select:{publishercode : true }, //this will fetch only particular column
		});
		res.json(pub);
	} catch (err) {
		res.json(err);
	}
};
export const Postpublisher = async (req: Request, res: Response) => {
	const {
		publishercode,
		publishertaxstatus,
		publishername,
		publishertaxidno,
		publishergstno,
		countryid,
		publishertypeid,
		publisherstatus,
		createdby,
		updatedby,
		updateddate,
		createddate,
	} = req.body;
	try {
		const country = await prisma.country.findUnique({
			where: {
				id: countryid,
			},
		});
		const publisherType = await prisma.publisherType.findUnique({
			where: {
				id: publishertypeid,
			},
		});
		if (country.id != null && publisherType.id != null) {
			const pub = await prisma.publisherMaster.create({
				data: {
					publishercode,
					publishertaxstatus,
					publishername,
					publishertaxidno,
					publishergstno,
					countryid,
					publishertypeid,
					publisherstatus,
					createdby,
					updatedby,
					updateddate,
					createddate,
				},
			});
			res.json(pub);
		}
		res.json(req);
	} catch (err) {
		res.json(err);
	}
};

export const Putpublisher = async (req: Request, res: Response) => {
	const {
		id,
		publishercode,
		publishertaxstatus,
		publishername,
		publishertaxidno,
		publishergstno,
		countryid,
		publishertypeid,
		publisherstatus,
		createdby,
		updatedby,
	} = req.body;
	try {
		const pub = await prisma.publisherMaster.update({
			where: {
				id,
			},
			data: {
				publishercode,
				publishertaxstatus,
				publishername,
				publishertaxidno,
				publishergstno,
				countryid,
				publishertypeid,
				publisherstatus,
				createdby,
				updatedby,
			},
		});
		res.json(pub);
	} catch (err) {
		res.json(err);
	}
};
// #endregion

// #region PublisherProfile
export const Getpublisherprofile = async (req: Request, res: Response) => {
	try {
		const pub = await prisma.publisherProfile.findMany();
		console.log(pub);
		res.json(pub);
	} catch (err) {
		res.json(err);
	}
};

export const Postpublisherprofile = async (req: Request, res: Response) => {
	const {
		publishermasterid,
		contactperson,
		mobile,
		mobileverified,
		emailverified,
		accountemailid,
		address1,
		address2,
		zipcode,
		country,
		logo,
		publisherdescription,
		whitelabelledsubdomainname,
		whitelabelledmobileappname,
		website,
		facebooklink,
		linkedinlink,
		termsncondition,
		publisherstatus,
		createdby,
		updatedby,
		createddate,
		updateddate,
	} = req.body;
	try {
		const publishermaster = await prisma.publisherMaster.findUnique({
			where: {
				id: publishermasterid,
			},
		});
		console.log(publishermaster);
		if (publishermaster.id != null) {
			const pub = await prisma.publisherProfile.create({
				data: {
					publishermasterid,
					contactperson,
					mobile,
					mobileverified,
					emailverified,
					accountemailid,
					address1,
					address2,
					zipcode,
					country,
					logo,
					publisherdescription,
					whitelabelledsubdomainname,
					whitelabelledmobileappname,
					website,
					facebooklink,
					linkedinlink,
					termsncondition,
					publisherstatus,
					createdby,
					updatedby,
					createddate,
					updateddate,
				},
			});
			console.log(pub);
			res.json(pub);
		}
		console.log(req);
		res.json(req);
	} catch (err) {
		console.log(err);
		res.json(req);
	}
};

export const Getpublisherprofilebyid = async (req: Request, res: Response) => {
	const { id } = req.params;
	try {
		const pub = await prisma.publisherProfile.findUnique({
			where: {
				id,
			},
		});
		res.json(pub);
	} catch (err) {
		res.json(err);
	}
};

export const Putpublisherprofile = async (req: Request, res: Response) => {
	const {
		id,
		publishermasterid,
		contactperson,
		mobile,
		mobileverified,
		emailverified,
		accountemailid,
		address1,
		address2,
		zipcode,
		country,
		logo,
		publisherdescription,
		whitelabelledsubdomainname,
		whitelabelledmobileappname,
		website,
		facebooklink,
		linkedinlink,
		termsncondition,
		publisherstatus,
		createdby,
		updatedby,
		createddate,
		updateddate,
	} = req.body;
	try {
		const pub = await prisma.publisherProfile.update({
			where: {
				id,
			},
			data: {
				publishermasterid,
				contactperson,
				mobile,
				mobileverified,
				emailverified,
				accountemailid,
				address1,
				address2,
				zipcode,
				country,
				logo,
				publisherdescription,
				whitelabelledsubdomainname,
				whitelabelledmobileappname,
				website,
				facebooklink,
				linkedinlink,
				termsncondition,
				publisherstatus,
				createdby,
				updatedby,
				createddate,
				updateddate,
			},
		});
		res.json(pub);
	} catch (err) {
		res.json(err);
	}
};
// #endregion
