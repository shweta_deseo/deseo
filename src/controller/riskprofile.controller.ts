import { Request, Response } from 'express';
import * as RPService from '../service/riskprofile.service';

export const getrpcategorytype = async (
	request: Request,
	response: Response,
) => {
	RPService.GetCategoryType(request, response);
};

export const postrpcategorytype = async (
	request: Request,
	response: Response,
) => {
	RPService.PostRPCategoryType(request, response);
};
