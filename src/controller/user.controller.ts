/* eslint-disable prettier/prettier */
import { Request, Response } from 'express';
import * as UserService from '../service/user.service';

const signup = async (request: Request, response: Response) => {
	UserService.signup(request, response);
};

const login = async (request: Request, response: Response) => {
	
	UserService.login(request, response);
};

const getuser = async (request: Request, response: Response) => {
	UserService.getuser(request, response);
};

export { signup, login, getuser };
