import { Request, Response } from 'express';
import * as PublisherService from '../service/publisher.service';

export const Getpublisher = async (request: Request, response: Response) => {
	PublisherService.Getpublisher(response);
};
export const Getpublisherbyid = async (
	request: Request,
	response: Response,
) => {
	PublisherService.Getpublisherbyid(request, response);
};

export const Postpublisher = async (request: Request, response: Response) => {
	PublisherService.Postpublisher(request, response);
};
export const Putpublisher = async (request: Request, response: Response) => {
	PublisherService.Putpublisher(request, response);
};

export const Getpublisherprofile = async (
	request: Request,
	response: Response,
) => {
	PublisherService.Getpublisherprofile(request, response);
};
export const Postpublisherprofile = async (
	request: Request,
	response: Response,
) => {
	PublisherService.Postpublisherprofile(request, response);
};
export const Getpublisherprofilebyid = async (
	request: Request,
	response: Response,
) => {
	PublisherService.Getpublisherprofilebyid(request, response);
};
export const Putpublisherprofile = async (
	request: Request,
	response: Response,
) => {
	PublisherService.Putpublisherprofile(request, response);
};
