import { Request, Response } from 'express';
import * as MasterService from '../service/master.service';

export const getservicetype = async (request: Request, response: Response) => {
	MasterService.GetServiceType(request, response);
};

export const postservicetype = async (request: Request, response: Response) => {
	MasterService.PostServiceType(request, response);
};

export const createAssetClass = async (
	request: Request,
	response: Response,
) => {
	MasterService.postAssetClass(request, response);
};

export const getAssetClass = async (request: Request, response: Response) => {
	MasterService.getAssetClass(request, response);
};

export const getAssetClassById = async (
	request: Request,
	response: Response,
) => {
	MasterService.getAssetClassById(request, response);
};

export const updateAssetClassById = async (
	request: Request,
	response: Response,
) => {
	MasterService.updateAssetClassById(request, response);
};

export const createPublisherType = async (
	request: Request,
	response: Response,
) => {
	MasterService.postPublisherType(request, response);
};

export const getpublisherType = async (
	request: Request,
	response: Response,
) => {
	MasterService.getPublisherType(request, response);
};

export const getPublisherTypeById = async (
	request: Request,
	response: Response,
) => {
	MasterService.getPublisherTypeById(request, response);
};
